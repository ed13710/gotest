package main

import (
    "github.com/go-martini/martini"
    "os/exec"
    "os"
//    "bytes"
//    "syscall"
    "fmt"
//    "time"
)

func main() {
    m := martini.Classic()
    m.Get("/", func() string {
        output, err := exec.Command("echo", "Executing a command in Go").CombinedOutput()
        if err != nil {
          os.Stderr.WriteString(err.Error())
        }
        fmt.Println(string(output))
        return string(output);
    })    
     m.RunOnAddr(":8080")
}